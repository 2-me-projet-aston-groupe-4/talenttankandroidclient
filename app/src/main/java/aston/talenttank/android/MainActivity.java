package aston.talenttank.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import android.text.method.ScrollingMovementMethod;
import android.view.View;

import java.util.List;

import aston.talenttank.android.activities.ListMission;
import aston.talenttank.android.activities.StartingPage;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.components.ui.login.LoginActivity;
import aston.talenttank.android.model.User;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private String BASE_URL = Constant.BASE_URL;
    private Button btnListMission;
    private TextView HomeText;
    private API api;
    private Intent intent;

    public void initHomePage () {
        HomeText = (TextView) findViewById(R.id.HomeTextView);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(API.class);

        Call<List<User>> users = api.getUsers();

        users.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                List<User> users = (List<User>) response.body();

                System.out.println("=================== USERS ===================");
                String content = "";
                for(User user : users) {
                    content += "user name = " + user.getFirstname() + "\n";
                }
                HomeText.setText(content);
                HomeText.setMovementMethod(new ScrollingMovementMethod());
                System.out.println("=================== END USERS ===================");
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

                System.out.println("Call : " + call.request());
                //System.out.println("Call : " + call.toString());
                HomeText.setText("Error ! \n" + t.getLocalizedMessage());
                System.out.println("Error ! \n" + t.getMessage());

            }
        });
    }
    /*
    private void defineActivityButtonListMission() {
        btnListMission = (Button) findViewById(R.id.btnListMission);
        btnListMission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivityListMission();
            }
        });
    }

    private void openActivityListMission() {
        Intent intent = new Intent(this, ListMission.class);
        startActivity(intent);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //setContentView(R.layout.activity_login);


        LoginRepository loginRepository = LoginRepository.getInstance();
        //loginRepository.
        //LoginRepository.getInstance();
        Boolean isLoggedIn = loginRepository.isLoggedIn();

        System.out.println("USER ==> " + loginRepository.getUser());
        System.out.println("ISLOGGEDIN ==> " + isLoggedIn);
        if(isLoggedIn) {
            intent = new Intent(this, StartingPage.class);
            //ViewGroup vg =
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
        /*
        LoginRepository loginRepository = LoginRepository.getInstance();
        String role = loginRepository.getUser().getRole();
        TextView tv = findViewById(R.id.starting_page_text_view);
        tv.setText(role);
         */
        //initHomePage();
        // setContentView(R.layout.activity_main);
        // initHomePage();
        // defineActivityButtonListMission();

    }

    public void startHome(View view) {
        // QUAND ON EST CONNECTE
        Intent intent = new Intent(MainActivity.this, StartingPage.class);
        MainActivity.this.startActivity(intent);
    }

}