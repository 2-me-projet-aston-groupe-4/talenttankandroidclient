package aston.talenttank.android.model;

public class Mission {
    private int idMission;
    private String label;
    private String author;
    private String description;
    private boolean isAvailable;
    private String creationDate;
    private int duration;
    private int budget;
    private int idProfessionnalCreator;
    private Category[] categories;
    private String userAccepted;
    private UserHasPostuled[] userPostuled;

    public Mission(int idMission, String label, String author, String description, boolean isAvailable, String creationDate, int duration, int budget, int idProfessionnalCreator, Category[] categories, String userAccepted, UserHasPostuled[] userPostuled) {
        this.idMission = idMission;
        this.label = label;
        this.author = author;
        this.description = description;
        this.isAvailable = isAvailable;
        this.creationDate = creationDate;
        this.duration = duration;
        this.budget = budget;
        this.idProfessionnalCreator = idProfessionnalCreator;
        this.categories = categories;
        this.userAccepted = userAccepted;
        this.userPostuled = userPostuled;
    }

    public String getUserAccepted() {
        return userAccepted;
    }

    public void setUserAccepted(String userAccepted) {
        this.userAccepted = userAccepted;
    }

    public UserHasPostuled[] getUserPostuled() {
        return userPostuled;
    }

    public void setUserPostuled(UserHasPostuled[] userPostuled) {
        this.userPostuled = userPostuled;
    }

    public int getIdMission() {
        return idMission;
    }

    public void setIdMission(int idMission) {
        this.idMission = idMission;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAuthor() { return author; }

    public void setAuthor(String author) { this.author = author; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) { this.isAvailable = isAvailable;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creation_date) {
        this.creationDate = creation_date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public Category[] getCategories() {
        return categories;
    }

    public void setCategories(Category[] categories) {
        this.categories = categories;
    }

    public int getIdProfessionnalCreator() {
        return idProfessionnalCreator;
    }

    public void setIdProfessionnalCreator(int idProfessionnalCreator) {
        this.idProfessionnalCreator = idProfessionnalCreator;
    }

    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }
}
