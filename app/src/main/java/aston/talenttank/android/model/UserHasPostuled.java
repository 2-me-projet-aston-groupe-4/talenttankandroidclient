package aston.talenttank.android.model;

public class UserHasPostuled {

    private int idFreelance;
    private int idMission;
    private String name;
    private int proposalPrice;
    private String proposalDate;

    public UserHasPostuled(int idFreelance, int idMission, String name, int proposalPrice, String proposalDate) {
        this.idFreelance = idFreelance;
        this.idMission = idMission;
        this.name = name;
        this.proposalPrice = proposalPrice;
        this.proposalDate = proposalDate;
    }

    public int getIdFreelance() {
        return idFreelance;
    }

    public void setIdFreelance(int idFreelance) {
        this.idFreelance = idFreelance;
    }

    public int getIdMission() {
        return idMission;
    }

    public void setIdMission(int idMission) {
        this.idMission = idMission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProposalPrice() {
        return proposalPrice;
    }

    public void setProposalPrice(int proposalPrice) {
        this.proposalPrice = proposalPrice;
    }

    public String getProposalDate() {
        return proposalDate;
    }

    public void setProposalDate(String proposalDate) {
        this.proposalDate = proposalDate;
    }
}
