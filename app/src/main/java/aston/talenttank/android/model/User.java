package aston.talenttank.android.model;

import java.io.Serializable;

public class User implements Serializable {

    private int iduser;
    private String firstname;
    private String lastname;
    private String password;
    private String email;
    private int phone;
    private int is_user_able_to_work;
    private int is_email_confirmed;
    private int is_user_active;
    private String role;
    private String access_token;
    private String error;

    public User(int iduser, String firstname, String lastname, String password, String email, int phone, int is_user_able_to_work, int is_email_confirmed, int is_user_active, String role) {
        this.iduser = iduser;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.is_user_able_to_work = is_user_able_to_work;
        this.is_email_confirmed = is_email_confirmed;
        this.is_user_active = is_user_active;
        this.role = role;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public void setIs_user_able_to_work(int is_user_able_to_work) {
        this.is_user_able_to_work = is_user_able_to_work;
    }

    public void setIs_email_confirmed(int is_email_confirmed) {
        this.is_email_confirmed = is_email_confirmed;
    }

    public void setIs_user_active(int is_user_active) {
        this.is_user_active = is_user_active;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getIduser() {
        return iduser;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getPhone() {
        return phone;
    }

    public int isIs_user_able_to_work() {
        return is_user_able_to_work;
    }

    public int isIs_email_confirmed() {
        return is_email_confirmed;
    }

    public int isIs_user_active() {
        return is_user_active;
    }

    public String getRole() {
        return role;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getErrorMessage() {
        return error;
    }
}
