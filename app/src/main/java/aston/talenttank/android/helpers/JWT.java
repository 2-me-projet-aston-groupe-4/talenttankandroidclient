package aston.talenttank.android.helpers;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;

public class JWT {
    public static String decodeJwt(String JWTToken) throws Exception {
        try {
            String[] split = JWTToken.split("\\.");
            Log.d("JWT DECODED ", "Body" + getJson(split[1]));
            return getJson(split[1]);
        } catch(UnsupportedEncodingException e) {
            //
        }
        return null;
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }
}
