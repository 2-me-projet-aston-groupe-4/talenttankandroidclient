package aston.talenttank.android.components.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import org.json.JSONObject;

import java.io.IOException;

import aston.talenttank.android.api.Auth;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.components.data.Result;
import aston.talenttank.android.components.data.model.LoggedInUser;
import aston.talenttank.android.R;
import aston.talenttank.android.helpers.JWT;
import aston.talenttank.android.model.User;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    private String BASE_URL = Constant.BASE_URL;
    private Auth auth;
    private Result<LoggedInUser> result = null;

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(String username, String password) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        auth = retrofit.create(Auth.class);

        System.out.println("USERNAME ==> " + username + " password ==> "+ password);
        User user = new User(username, password);
        Call<User> call = auth.login(user);
        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User r = response.body();
                if(r.getErrorMessage() != null) {
                    //result = new Result.Error(new IOException(r.getErrorMessage(), null));
                    loginResult.setValue(new LoginResult(R.string.login_failed));
                } else {
                    if(r.getAccess_token() != null) {
                        try {
                            String decodedToken = JWT.decodeJwt(r.getAccess_token());
                            JSONObject data = new JSONObject(decodedToken);
                            String userData = data.getString("user");
                            String userName = new JSONObject(userData).getString("firstname");
                            String lastName = new JSONObject(userData).getString("lastname");
                            String email = new JSONObject(userData).getString("email");
                            String phone = new JSONObject(userData).getString("phone");
                            String role = new JSONObject(userData).getString("role");
                            Integer userId = new JSONObject(userData).getInt("iduser");
                            System.out.println(userId + " " + userId.toString());

                            LoggedInUser loggedInUser = new LoggedInUser(userId.toString(), userName, lastName, email, phone, role, data.toString());
                            result = new Result.Success<>(loggedInUser);
                            //LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
                            loginResult.setValue(new LoginResult(new LoggedInUserView(userName)));
                            loginRepository.setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
                        } catch (Exception e) {
                            e.printStackTrace();
                            //result = new Result.Error(new IOException("Une erreur s'est produite", null));
                            loginResult.setValue(new LoginResult(R.string.login_failed));
                        }
                    }
                }
                System.out.println("success " + response.body().getAccess_token() + " RESULTT => " + result);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                //result = new Result.Error(new IOException("Une erreur s'est produite", null));
                loginResult.setValue(new LoginResult(R.string.login_failed));
                System.out.println("failededed " + t);
            }
        });
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}