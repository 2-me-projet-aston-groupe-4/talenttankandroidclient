package aston.talenttank.android.components.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    private String userId;
    private String displayName;
    private String lastName;
    private String email;
    private String phone;
    private String role;
    private String accessToken;

    public LoggedInUser(String userId, String displayName) {
        this.userId = userId;
        this.displayName = displayName;
    }

    public LoggedInUser(String userId, String displayName, String lastName, String email, String phone, String role, String accessToken) {
        this.userId = userId;
        this.displayName = displayName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getRole() {
        return role;
    }

    public String getAccessToken() {
        return accessToken;
    }
}