package aston.talenttank.android.components.data;

import android.os.AsyncTask;
import android.support.v4.os.IResultReceiver;

import org.json.JSONObject;

import aston.talenttank.android.api.API;
import aston.talenttank.android.api.Auth;
import aston.talenttank.android.components.data.model.LoggedInUser;
import aston.talenttank.android.helpers.JWT;
import aston.talenttank.android.model.APIResponse;
import aston.talenttank.android.model.User;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource extends AsyncTask<String, Result<LoggedInUser>, Result<LoggedInUser>> {
    private Auth auth;
    private Result<LoggedInUser> result = null;

    public LoginDataSource() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        auth = retrofit.create(Auth.class);
    }

    /*@Override
    protected Result<LoggedInUser> doInBackground(Object[] objects) {
        return null;
    }*/

    @Override
    protected Result<LoggedInUser> doInBackground(String...params) {
        System.out.println("USERNAME ==> " + params[0] + " password ==> "+ params[1]);
        User user = new User(params[0], params[1]);
        Call<User> call = auth.login(user);
        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User r = response.body();
                if(r.getErrorMessage() != null) {
                    result = new Result.Error(new IOException(r.getErrorMessage(), null));
                } else {
                    if(r.getAccess_token() != null) {
                        try {
                            String decodedToken = JWT.decodeJwt(r.getAccess_token());
                            JSONObject data = new JSONObject(decodedToken);
                            String userData = data.getString("user");
                            String userName = new JSONObject(userData).getString("firstname");
                            Integer userId = new JSONObject(userData).getInt("iduser");
                            System.out.println(userId + " " + userId.toString());

                            LoggedInUser loggedInUser = new LoggedInUser(java.util.UUID.randomUUID().toString(), userName);
                            result = new Result.Success<>(loggedInUser);
                        } catch (Exception e) {
                            e.printStackTrace();
                            result = new Result.Error(new IOException("Une erreur s'est produite", null));
                        }
                    }
                }
                System.out.println("success " + response.body().getAccess_token() + " RESULTT => " + result);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                result = new Result.Error(new IOException("Une erreur s'est produite", null));
                System.out.println("failededed " + t);
            }
        });
        return result;
    }

    public Result<LoggedInUser> login(String username, String password) throws InterruptedException {
        /*System.out.println("USERNAME ==> " + username + " password ==> "+ password);
        User user = new User(username, password);
        Call<User> call = auth.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User r = response.body();
                if(r.getErrorMessage() != null) {
                    result = new Result.Error(new IOException(r.getErrorMessage(), null));
                } else {
                    if(r.getAccess_token() != null) {
                        try {
                            String decodedToken = JWT.decodeJwt(r.getAccess_token());
                            JSONObject data = new JSONObject(decodedToken);
                            String userData = data.getString("user");
                            String userName = new JSONObject(userData).getString("firstname");
                            Integer userId = new JSONObject(userData).getInt("iduser");
                            System.out.println(userId + " " + userId.toString());

                            LoggedInUser loggedInUser = new LoggedInUser(java.util.UUID.randomUUID().toString(), userName);
                            result = new Result.Success<>(loggedInUser);
                        } catch (Exception e) {
                            e.printStackTrace();
                            result = new Result.Error(new IOException("Une erreur s'est produite", null));
                        }
                    }
                }
                System.out.println("success " + response.body().getAccess_token());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                result = new Result.Error(new IOException("Une erreur s'est produite", null));
                System.out.println("failededed " + t);
            }
        });*/
        /*try {
            // TODO: handle loggedInUser authentication
            LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }*/

        /*try {
            Thread.sleep(2000);
            if(result != null) {
                System.out.println("res => " + result);
                return result;
            }
            return new Result.Error(new IOException("ERROORRR", null));
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }*/
        doInBackground(username, password);
        TimeUnit.MINUTES.sleep(1);
        System.out.println("RESULTTTT +==> "+ result);
        if(result == null) {
            LoginDataSource login = new LoginDataSource();
            login.execute(username, password);

            return result;
        } else {
            return result;
        }
    }

    @Override
    protected void onPostExecute(Result<LoggedInUser> result ) {
        //TODO: After async task finished based on task success
        //(Change activity, hide progressbar...)
        /*if(result instanceof Result.Success)
            loginAction();*/
        if (result instanceof Result.Success) {
            loginAction();
            //LoginRepository lr = new LoginRepository(this);
            //lr.setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
        }
    }

    public Result<LoggedInUser> loginAction() {
        try {
            return result;
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}