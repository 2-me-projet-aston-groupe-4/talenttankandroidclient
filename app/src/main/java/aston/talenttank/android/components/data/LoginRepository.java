package aston.talenttank.android.components.data;

import android.content.Intent;

import java.io.IOException;

import javax.security.auth.login.LoginException;

import aston.talenttank.android.components.data.model.LoggedInUser;
import aston.talenttank.android.components.ui.login.LoginActivity;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private LoggedInUser user = null;

    // private constructor : singleton access
    public LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }
    public LoginRepository() {}

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public static LoginRepository getInstance() {
        if(instance == null) {
            return new LoginRepository();
        }
        return instance;
    }

    public void logout() {
        user = null;
    }

    public void setLoggedInUser(LoggedInUser user) {
        this.user = user;
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    public LoggedInUser getUser() {
        return this.user;
    }

    public Result<LoggedInUser> login(String username, String password) {
    //public void login(String username, String password) {
        // handle login
        //dataSource.login(username, password);
        try {
            //Thread.sleep(1000);
            Result<LoggedInUser> result = dataSource.doInBackground(username, password);
            //Result<LoggedInUser> result = dataSource.onPostExecute(); //(username, password);
        /*if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
        }*/
            return result;
        } catch (Exception e){
            return new Result.Error(new IOException("Error logging in", e));
        }
    }
}