package aston.talenttank.android.api;

import java.util.ArrayList;
import java.util.List;

import aston.talenttank.android.model.User;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.UserHasPostuled;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface API {
    @GET("user/getAll/")
    //Call<User> getUsers();
    Call<List<User>> getUsers();

    @GET("missions")
    Call<List<Mission>> getAllMissions();

    @GET("missions/{id}")
    Call<List<Mission>> getOneMission(@Path("id") int id);

    @GET("missions/pro/{id}")
    Call<List<Mission>> getAllMissionCreatedByProfessionnal(@Path("id") int id);

    @DELETE("missions/pro/delete/{id}")
    Call<Void> deleteMission(@Path("id") int id);

    @POST("/missions/pro/creation")
    Call<Mission> createMission(@Body Mission mission);

    @POST("/missions/freelance/candidate")
    Call<UserHasPostuled> candidate(@Body UserHasPostuled dataCandidate);
}
