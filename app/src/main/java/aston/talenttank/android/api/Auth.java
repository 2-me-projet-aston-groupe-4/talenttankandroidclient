package aston.talenttank.android.api;

import java.util.List;

import aston.talenttank.android.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Auth {
    @POST("/login")
    Call<User> login(@Body User user);
}
