package aston.talenttank.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.UserHasPostuled;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Postulate extends AppCompatActivity {

    private void candidate(UserHasPostuled dataCandidate) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);
        Call<UserHasPostuled> call = api.candidate(dataCandidate);
        call.enqueue(new Callback<UserHasPostuled>() {
            @Override
            public void onResponse(Call<UserHasPostuled> call, Response<UserHasPostuled> response) {

            }

            @Override
            public void onFailure(Call<UserHasPostuled> call, Throwable t) {

            }
        });
    }

    private void onCLickPostulate() {
        TextView viewPrice = findViewById(R.id.proposal_price_freelance);
        if (!viewPrice.getText().toString().equals("")) {
            Intent intent = this.getIntent();
            String data = intent.getExtras().getString("DATA");
            int idFreelance = Integer.parseInt(data.split("-")[0]);
            int idMission = Integer.parseInt(data.split("-")[1]);
            int newPrice = Integer.parseInt(viewPrice.getText().toString());
             UserHasPostuled dataCandidate = new UserHasPostuled(idFreelance, idMission, "John", newPrice, "2020-10-20");
            candidate(dataCandidate);
            Toast.makeText(getApplicationContext(), "Candidature envoyée", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, ListMission.class);
            startActivity(i);
        } else {
            Toast.makeText(getApplicationContext(), "Veuillez saisir un montant", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postulate_freelance);
        Button btnPostulate = findViewById(R.id.btn_postulate_submit);
        Intent intent = this.getIntent();
        String currentPrice = intent.getExtras().getString("DATA").split("-")[2];
        TextView viewPrice = findViewById(R.id.proposal_price_freelance);
        viewPrice.setText(currentPrice);

        btnPostulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCLickPostulate();
            };
        });
    }
}
