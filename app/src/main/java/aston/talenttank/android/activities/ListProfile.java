package aston.talenttank.android.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import java.util.ArrayList;
import java.util.List;

import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.User;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListProfile extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private ListView listViewProfiles;
    private ListAdapter adapter;
    private List<User> missions = new ArrayList<>();

    DrawerLayout drawerLayout;


    private void getListProfiles() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<List<User>> call = (Call<List<User>>) api.getUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code: " + response.code());
                    return;
                }
                List<User> responseJson = (List<User>) response.body();
                System.out.println(responseJson);
                populateListView(responseJson);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                System.out.println("Call : " + call.request());
                System.out.println("Error ! \n" + t.getMessage());
            }
        });
    }

    public class MissionAdapter extends BaseAdapter {
        private Context context;
        private List<User> listProfile;
        private LayoutInflater inflater;

        public MissionAdapter(Context context, List<User> listProfile) {
            this.context = context;
            this.listProfile = listProfile;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return this.listProfile.size();
        }

        @Override
        public User getItem(int i) {
            return this.listProfile.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.card_mission, viewGroup, false);
            }

            TextView firstname = view.findViewById(R.id.value_user_firstname);
            TextView lastname = view.findViewById(R.id.value_user_lastname);
            TextView category = view.findViewById(R.id.value_user_category);


            final User currentProfile = this.listProfile.get(i);
            firstname.setText(currentProfile.getFirstname());
            lastname.setText(currentProfile.getLastname());
            category.setText(String.valueOf(currentProfile.getFirstname()));
            TextView circleIsAvailable = view.findViewById(R.id.value_isAvailable_mission);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("*******************************************************");
                    openDetailMission(currentProfile.getIduser());
                }
            });
            return view;
        }
    }

    private void populateListView(List<User> jsonMissions) {
        listViewProfiles = (ListView) findViewById(R.id.list_mission);
        adapter = new MissionAdapter(this, jsonMissions);
        listViewProfiles.setAdapter(adapter);
    }

    private void openDetailMission(int idMission) {
        Intent intent = new Intent(ListProfile.this, DetailMissionFreelance.class);
        System.out.println(String.valueOf(idMission));
        intent.putExtra("idMission", String.valueOf(idMission));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_mission);
        drawerLayout = findViewById(R.id.drawer_layout);
        getListProfiles();
    }

    public void ClickMenu (View v){
        OverlayActivity.openDrawer(drawerLayout);
    }
    public void ClickLogout (){
        OverlayActivity.closeDrawer(drawerLayout);
    }

    public void ClickHome(View v){
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }

    public void ClickMissions(View v){
        recreate();
    }

    public void ClickAboutMe (View v) {

        OverlayActivity.redirectActivity(this, StartingPage.class);
    }

    public void ClickProfiles (View v){
        recreate();
    }
}
