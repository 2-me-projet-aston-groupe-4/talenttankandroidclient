package aston.talenttank.android.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListMissionPro extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private ListView listViewMissions;
    private ListAdapter adapter;
    private List<Mission> missions = new ArrayList<>();
    DrawerLayout drawerLayout;
    private String idUser = LoginRepository.getInstance().getUser().getUserId();

    private void getListMissions() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<List<Mission>> call = (Call<List<Mission>>) api.getAllMissionCreatedByProfessionnal(Integer.parseInt(idUser));
        call.enqueue(new Callback<List<Mission>>() {
            @Override
            public void onResponse(Call<List<Mission>> call, Response<List<Mission>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code: " + response.code());
                    return;
                }
                List<Mission> responseJson = (List<Mission>) response.body();
                System.out.println(responseJson);
                populateListView(responseJson);
            }

            @Override
            public void onFailure(Call<List<Mission>> call, Throwable t) {
                System.out.println("Call : " + call.request());
                System.out.println("Error ! \n" + t.getMessage());
            }
        });
    }

    public class MissionAdapter extends BaseAdapter {
        private Context context;
        private List<Mission> listMission;
        private LayoutInflater inflater;
        private String userRole = LoginRepository.getInstance().getUser().getRole();

        public MissionAdapter(Context context, List<Mission> listMission) {
            this.context = context;
            this.listMission = listMission;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return this.listMission.size();
        }

        @Override
        public Mission getItem(int i) {
            return this.listMission.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.card_mission, viewGroup, false);
            }

            TextView label = view.findViewById(R.id.value_label_mission);
            TextView freelanceAccepted = view.findViewById(R.id.value_author_mission);
            TextView budget = view.findViewById(R.id.value_budget_mission);
            TextView duration = view.findViewById(R.id.value_duration_mission);
            TextView date = view.findViewById(R.id.value_date_mission);
            TextView viewPostuled = view.findViewById(R.id.list_postuled_mission_pro);

            final Mission currentMission = this.listMission.get(i);
            label.setText(currentMission.getLabel());
            freelanceAccepted.setText(currentMission.getUserAccepted());
            budget.setText(String.valueOf(currentMission.getBudget()) + "€");
            duration.setText(String.valueOf(currentMission.getDuration()) + " jours");
            date.setText(currentMission.getCreationDate());

            TextView circleIsAvailable = view.findViewById(R.id.value_isAvailable_mission);
            if (currentMission.isAvailable() == false) {
                circleIsAvailable.setBackgroundResource(R.drawable.circle_not_available);
            } else {
                circleIsAvailable.setBackgroundResource(R.drawable.circle_available);
            }

            if (currentMission.getUserPostuled().length > 1) {
                viewPostuled.setText(currentMission.getUserPostuled().length + " candidatures en cours");
            } else if (currentMission.getUserPostuled().length == 1) {
                viewPostuled.setText(currentMission.getUserPostuled().length + " candidature en cours");
            } else {
                viewPostuled.setText("Aucune candidature en cours");
            }

            if (!currentMission.isAvailable()) {
                viewPostuled.setVisibility(View.INVISIBLE);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openDetailMission(currentMission.getIdMission());
                }
            });

            return view;
        }
    }

    private void populateListView(List<Mission> jsonMissions) {
        listViewMissions = (ListView) findViewById(R.id.list_mission_pro);
        adapter = new MissionAdapter(this, jsonMissions);
        listViewMissions.setAdapter(adapter);
    }

    private void openDetailMission(int idMission) {
        Intent intent = new Intent(ListMissionPro.this, DetailMissionPro.class);
        intent.putExtra("idMission", String.valueOf(idMission));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_mission_pro);
        drawerLayout = findViewById(R.id.drawer_layout);
        getListMissions();
    }

    public void ClickHome(View v){
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }
    public void ClickMenu (View v){
        OverlayActivity.openDrawer(drawerLayout);
    }
    public void ClickMissions(View v){
        recreate();
        //user for ?


    }
    public void ClickAboutMe (View v){
        // WIP still redirecting to Starting page
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }
    public void ClickLogout (){
        OverlayActivity.closeDrawer(drawerLayout);
    }

}

