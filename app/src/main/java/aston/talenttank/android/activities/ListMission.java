package aston.talenttank.android.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;


import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.components.ui.login.LoginActivity;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListMission extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private ListView listViewMissions;
    private ListAdapter adapter;
    private List<Mission> missions = new ArrayList<>();

    DrawerLayout drawerLayout;


    private void getListMissions() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<List<Mission>> call = (Call<List<Mission>>) api.getAllMissions();
        call.enqueue(new Callback<List<Mission>>() {
            @Override
            public void onResponse(Call<List<Mission>> call, Response<List<Mission>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code: " + response.code());
                    return;
                }
                List<Mission> responseJson = (List<Mission>) response.body();
                populateListView(responseJson);
            }

            @Override
            public void onFailure(Call<List<Mission>> call, Throwable t) {
                System.out.println("Call : " + call.request());
                System.out.println("Error ! \n" + t.getMessage());
            }
        });
    }

    public class MissionAdapter extends BaseAdapter {
        private Context context;
        private List<Mission> listMission;
        private LayoutInflater inflater;

        public MissionAdapter(Context context, List<Mission> listMission) {
            this.context = context;
            this.listMission = listMission;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return this.listMission.size();
        }

        @Override
        public Mission getItem(int i) {
            return this.listMission.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.card_mission, viewGroup, false);
            }

            TextView label = view.findViewById(R.id.value_label_mission);
            TextView author = view.findViewById(R.id.value_author_mission);
            TextView budget = view.findViewById(R.id.value_budget_mission);
            TextView duration = view.findViewById(R.id.value_duration_mission);
            TextView date = view.findViewById(R.id.value_date_mission);
            TextView viewPostuled = view.findViewById(R.id.list_postuled_mission_pro);

            final Mission currentMission = this.listMission.get(i);
            label.setText(currentMission.getLabel());
            author.setText(currentMission.getAuthor());
            budget.setText(String.valueOf(currentMission.getBudget()) + "€");
            duration.setText(String.valueOf(currentMission.getDuration()) + " jours");
            date.setText(currentMission.getCreationDate());

            TextView circleIsAvailable = view.findViewById(R.id.value_isAvailable_mission);
            if (currentMission.isAvailable() == false) {
                circleIsAvailable.setBackgroundResource(R.drawable.circle_not_available);
            } else {
                circleIsAvailable.setBackgroundResource(R.drawable.circle_available);
            }

            if (currentMission.getUserPostuled().length > 1) {
                viewPostuled.setText(currentMission.getUserPostuled().length + " candidatures en cours");
            } else if (currentMission.getUserPostuled().length == 1) {
                viewPostuled.setText(currentMission.getUserPostuled().length + " candidature en cours");
            } else {
                viewPostuled.setText("Aucune candidature en cours");
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openDetailMission(currentMission.getIdMission());
                }
            });
            return view;
        }
    }

    private void populateListView(List<Mission> jsonMissions) {
        listViewMissions = (ListView) findViewById(R.id.list_mission);
        adapter = new MissionAdapter(this, jsonMissions);
        listViewMissions.setAdapter(adapter);
    }

    private void openDetailMission(int idMission) {
        Intent intent = new Intent(ListMission.this, DetailMissionFreelance.class);
        intent.putExtra("idMission", String.valueOf(idMission));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_mission);
        drawerLayout = findViewById(R.id.drawer_layout);
        getListMissions();
    }

    public void ClickMenu (View v){
        OverlayActivity.openDrawer(drawerLayout);
    }
    public void ClickLogout (){
        OverlayActivity.closeDrawer(drawerLayout);
    }

    public void ClickHome(View v){
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }

    public void ClickMissions(View v){
        recreate();
        //user for ?


    }

    public void ClickLogout (View view) {
        OverlayActivity.closeDrawer(drawerLayout);
        LoginRepository loginRepository = LoginRepository.getInstance();
        loginRepository.logout();
        OverlayActivity.redirectActivity(this, LoginActivity.class);
    }

    public void ClickAboutMe (View v){
        // WIP still redirecting to Starting page
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }

}
