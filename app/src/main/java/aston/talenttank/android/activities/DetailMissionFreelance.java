package aston.talenttank.android.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.model.Category;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.UserHasPostuled;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailMissionFreelance extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private TextView label;
    private TextView author;
    private TextView description;
    private TextView date;
    private TextView duration;
    private TextView budget;
    private TextView isAvailable;
    private TextView categories;
    private UserHasPostuled[] listUsersPostuled;
    private Mission currentMission;

    private void getMissionToDisplay(int id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<List<Mission>> call = (Call<List<Mission>>) api.getOneMission(id);
        call.enqueue(new Callback<List<Mission>>() {
            @Override
            public void onResponse(Call<List<Mission>> call, Response<List<Mission>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code: " + response.code());
                    return;
                }
                List<Mission> responseJson = (List<Mission>) response.body();
                populateMissionView(responseJson);
            }

            @Override
            public void onFailure(Call<List<Mission>> call, Throwable t) {
                System.out.println("Call : " + call.request());
                System.out.println("Error ! \n" + t.getMessage());
            }
        });
    }

    private void populateMissionView(List<Mission> missions) {
        this.currentMission = missions.get(0);
        label = findViewById(R.id.detail_label_mission);
        label.setText(missions.get(0).getLabel());
        author = findViewById(R.id.detail_author_mission);
        author.setText(missions.get(0).getAuthor());
        description = findViewById(R.id.detail_description_mission);
        description.setText(missions.get(0).getDescription());
        budget = findViewById(R.id.detail_budget_mission);
        budget.setText(String.valueOf(missions.get(0).getBudget()) + " €");
        duration = findViewById(R.id.detail_duration_mission);
        duration.setText(String.valueOf(missions.get(0).getDuration()) + " jours");
        date = findViewById(R.id.detail_date_mission);
        date.setText(String.valueOf(missions.get(0).getCreationDate()));
        isAvailable = findViewById(R.id.detail_isAvailable_mission);
        if (missions.get(0).isAvailable()) {
            isAvailable.setBackgroundResource(R.drawable.circle_available);
        } else {
            isAvailable.setBackgroundResource(R.drawable.circle_not_available);
        }
        categories = findViewById(R.id.detail_categories_mission);
        Category[] categoriesList = missions.get(0).getCategories();
        String contentCategories = "";
        int index = 0;
        for (Category category : categoriesList) {
            if (index == categoriesList.length - 1) {
                contentCategories+= category.getLabel();
            } else {
                contentCategories+= category.getLabel() + " - ";
            }
            index++;
        }
        categories.setText(contentCategories);

        Button btnUsersPostuled = findViewById(R.id.btn_display_postuled);
        listUsersPostuled = missions.get(0).getUserPostuled();
        String content = "";
        if (listUsersPostuled.length > 1) {
            content = listUsersPostuled.length + " candidatures en cours";
        } else if (listUsersPostuled.length == 1) {
            content = "1 candidature en cours";
        } else {
            content = "Aucune candidature en cours";
        }
        btnUsersPostuled.setText(content);

        Button btnPostulate = findViewById(R.id.btn_postulate_mission);
        if (!missions.get(0).isAvailable()) {
            btnPostulate.setVisibility(View.INVISIBLE);
            btnPostulate.setHeight(0);
        }
    }

    private void onClickRedirectctivityPostulate() {
        Intent intent = new Intent(this, Postulate.class);
        String idMission = String.valueOf(this.currentMission.getIdMission());
        String idFreelance = LoginRepository.getInstance().getUser().getUserId();
        String currentPrice = String.valueOf(this.currentMission.getBudget());
        String currentDate = String.valueOf(this.currentMission.getCreationDate());
        String data = idFreelance + "-" + idMission + "-" + currentPrice + "-" + currentDate;
        intent.putExtra("DATA", data);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_mission_freelance);
        Intent i = this.getIntent();
        int currentIdMission = Integer.parseInt(i.getExtras().getString("idMission"));
        getMissionToDisplay(currentIdMission);
        Button btnPostulate = findViewById(R.id.btn_postulate_mission);
        btnPostulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRedirectctivityPostulate();
            };
        });
    }
}

