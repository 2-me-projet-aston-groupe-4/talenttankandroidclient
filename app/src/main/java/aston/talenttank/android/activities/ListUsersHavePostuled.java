package aston.talenttank.android.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.User;
import aston.talenttank.android.model.UserHasPostuled;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListUsersHavePostuled extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private ListView listUsersHavePostuled;
    private ListAdapter adapter;
    private List<Mission> missions = new ArrayList<>();
    DrawerLayout drawerLayout;
    private String idUser = LoginRepository.getInstance().getUser().getUserId();

    public class ListPostuledAdapter extends BaseAdapter {
        private Context context;
        private List<UserHasPostuled> listUsersPostuled;
        private LayoutInflater inflater;

        public ListPostuledAdapter(Context context, List<UserHasPostuled> listUsersPostuled) {
            this.context = context;
            this.listUsersPostuled = listUsersPostuled;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return this.listUsersPostuled.size();
        }

        @Override
        public UserHasPostuled getItem(int i) {
            return this.listUsersPostuled.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.card_users_have_postuled, viewGroup, false);
            }
            final UserHasPostuled currentUserPostuled = this.listUsersPostuled.get(i);
            TextView viewNamePostuled = view.findViewById(R.id.name_postuled);
            TextView viewPricePostuled = view.findViewById(R.id.price_postuled);
            TextView viewDatePostuled = view.findViewById(R.id.date_postuled);
            viewNamePostuled.setText(currentUserPostuled.getName());
            viewPricePostuled.setText(String.valueOf(currentUserPostuled.getProposalPrice()) + "€");
            viewDatePostuled.setText(currentUserPostuled.getProposalDate());
            return view;
        }
    }

    private void populateListView(List<UserHasPostuled> data) {
        listUsersHavePostuled = (ListView) findViewById(R.id.list_users_postuled);
        adapter = new ListPostuledAdapter(this, data);
        listUsersHavePostuled.setAdapter(adapter);
    }

    private List<UserHasPostuled> getListFromData() {
        List<UserHasPostuled> list = new ArrayList<>();
        Intent intent = this.getIntent();
        String data = intent.getExtras().getString("POSTULED");
        String[] arrayData = data.split("_");
        for (int i = 0; i < arrayData.length; i++) {
            int idFreelance = Integer.parseInt(arrayData[i].split("-")[0]);
            int idMission = Integer.parseInt(arrayData[i].split("-")[1]);
            String name = arrayData[i].split("-")[2];
            int proposalPrice = Integer.parseInt(arrayData[i].split("-")[3]);
            String proposalDate = arrayData[i].split("-")[4];
            list.add(new UserHasPostuled(idFreelance, idMission, name, proposalPrice, proposalDate));
        }
        return list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_users_have_postuled);
        drawerLayout = findViewById(R.id.drawer_layout);
        populateListView(getListFromData());
    }

    public void ClickHome(View v){
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }
    public void ClickMenu (View v){
        OverlayActivity.openDrawer(drawerLayout);
    }
    public void ClickMissions(View v){
        recreate();
        //user for ?


    }
    public void ClickAboutMe (View v){
        // WIP still redirecting to Starting page
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }
    public void ClickLogout (){
        OverlayActivity.closeDrawer(drawerLayout);
    }
}

