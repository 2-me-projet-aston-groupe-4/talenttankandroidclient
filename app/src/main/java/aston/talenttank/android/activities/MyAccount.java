package aston.talenttank.android.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import aston.talenttank.android.R;
import aston.talenttank.android.components.data.LoginRepository;

public class MyAccount extends AppCompatActivity {

    DrawerLayout drawerLayout;
    LoginRepository loginRepository = LoginRepository.getInstance();
    String username = loginRepository.getUser().getDisplayName();
    String lastname = loginRepository.getUser().getLastName();
    String email = loginRepository.getUser().getEmail();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);



        TextView tv = findViewById(R.id.plain_text_input);
        tv.setText(username);
        TextView tv2 = findViewById(R.id.plain_text_input2);
        tv2.setText(lastname);
        TextView tv3 = findViewById(R.id.plain_text_input3);
        
        tv3.setText(email);
        drawerLayout = findViewById(R.id.drawer_layout);
    }


    public void ClickMenu (View v){
        System.out.println("drawer menu");
        OverlayActivity.openDrawer(drawerLayout);
    }
    public void ClickLogout (){
        OverlayActivity.closeDrawer(drawerLayout);
    }

    public void ClickHome(View v){
        System.out.println("return starting page");
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }

    public void ClickMissions(View v){
        recreate();
        //user for ?
    }

    public void ClickAboutMe (View v){
        // WIP still redirecting to Starting page
        OverlayActivity.redirectActivity(this, StartingPage.class);
    }


}