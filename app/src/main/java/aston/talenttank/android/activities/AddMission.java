package aston.talenttank.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.model.Category;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.UserHasPostuled;
import aston.talenttank.android.utils.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddMission extends AppCompatActivity {

    private void addMission(Mission mission) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);
        Call<Mission> call = api.createMission(mission);
        call.enqueue(new Callback<Mission>() {
            @Override
            public void onResponse(Call<Mission> call, Response<Mission> response) {

            }
            @Override
            public void onFailure(Call<Mission> call, Throwable t) {

            }
        });
    }

    private void onClickAddMission() {
        EditText editLabel = findViewById(R.id.add_label_mission);
        EditText editDescription = findViewById(R.id.add_description_mission);
        EditText editDuration = findViewById(R.id.add_duration_mission);
        EditText editBudget = findViewById(R.id.add_budget_mission);
        Spinner spSkills = findViewById(R.id.select_skill);
        Spinner spLevel = findViewById(R.id.select_level);

        String newLabel = editLabel.getText().toString();
        String newDescription = editDescription.getText().toString();
        String newDuration = editDuration.getText().toString();
        String newBudget = editBudget.getText().toString();
        String newSkill = spSkills.getSelectedItem().toString();
        String newLevel= spLevel.getSelectedItem().toString();

        if (!newLabel.equals("") && !newDescription.equals("") && !newDuration.equals("")  && !newBudget.equals("") && !newSkill.equals("") && !newLevel.equals("")) {
            Category[] newCategories = {new Category(defineIdCategory(newSkill), newSkill, ""), new Category(defineIdCategory(newLevel), newLevel, "")};
            UserHasPostuled[] newUsersHavePostuled = {};
            int idUser = Integer.parseInt(LoginRepository.getInstance().getUser().getUserId());
            Mission newMission = new Mission(0, newLabel, "john", newDescription, false, "", Integer.parseInt(newDuration), Integer.parseInt(newBudget), idUser, newCategories, "", newUsersHavePostuled);

            for (int i = 0; i < newCategories.length; i++) {
                System.out.println(newCategories[i].getLabel());
            }

            addMission(newMission);
            Toast.makeText(getApplicationContext(), "Mission créée", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, ListMissionPro.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Tous les champs doivent être remplis", Toast.LENGTH_SHORT).show();
        }
    }

    private int defineIdCategory(String label) {
        int id = 0;
        if (label.equals("Front")) {
            id = 1;
        } else if (label.equals("Back")) {
            id = 2;
        } else if (label.equals("Fullstack")) {
            id = 3;
        } else if (label.equals("DevOps")) {
            id = 4;
        } else if (label.equals("Admin-Systeme")) {
            id = 5;
        } else if (label.equals("Junior")) {
            id = 6;
        } else if (label.equals("Débutant")) {
            id = 7;
        } else if (label.equals("Confirmé")) {
            id = 8;
        } else if (label.equals("Sénior")) {
            id = 9;
        }
        return id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_mission);

        Spinner spSkills = findViewById(R.id.select_skill);
        Spinner spLevel = findViewById(R.id.select_level);

        ArrayAdapter<CharSequence> adapterSkill = ArrayAdapter.createFromResource(this, R.array.select_skills, android.R.layout.simple_spinner_item);
        adapterSkill.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSkills.setAdapter(adapterSkill);

        ArrayAdapter<CharSequence> adapterLevel = ArrayAdapter.createFromResource(this, R.array.select_level, android.R.layout.simple_spinner_item);
        adapterLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLevel.setAdapter(adapterLevel);

        Button btnAddMission = findViewById(R.id.btn_add_mission);
        btnAddMission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAddMission();
            };
        });
    }
}
