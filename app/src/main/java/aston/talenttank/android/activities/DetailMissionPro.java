package aston.talenttank.android.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import aston.talenttank.android.R;
import aston.talenttank.android.api.API;
import aston.talenttank.android.model.Category;
import aston.talenttank.android.model.Mission;
import aston.talenttank.android.model.UserHasPostuled;
import aston.talenttank.android.utils.Constant;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailMissionPro extends AppCompatActivity {
    private API api;
    private String BASE_URL = Constant.BASE_URL;
    private TextView label;
    private TextView description;
    private TextView date;
    private TextView duration;
    private TextView budget;
    private TextView isAvailable;
    private TextView userAccepted;
    private Button btnUsersPostuled;
    private TextView categories;
    private UserHasPostuled[] listUsersPostuled;
    private boolean missionIsAvailable;

    private void getMissionToDisplay(int id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<List<Mission>> call = (Call<List<Mission>>) api.getOneMission(id);
        call.enqueue(new Callback<List<Mission>>() {
            @Override
            public void onResponse(Call<List<Mission>> call, Response<List<Mission>> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code: " + response.code());
                    return;
                }
                List<Mission> responseJson = (List<Mission>) response.body();
                populateMissionView(responseJson);
            }

            @Override
            public void onFailure(Call<List<Mission>> call, Throwable t) {
                System.out.println("Call : " + call.request());
                System.out.println("Error ! \n" + t.getMessage());
            }
        });
    }

    private void populateMissionView(List<Mission> missions) {
        label = findViewById(R.id.label_mission_pro);
        label.setText(missions.get(0).getLabel());
        userAccepted = findViewById(R.id.accepted_mission_pro);
        userAccepted.setText(missions.get(0).getUserAccepted());
        description = findViewById(R.id.description_mission_pro);
        description.setText(missions.get(0).getDescription());
        budget = findViewById(R.id.budget_mission_pro);
        budget.setText(String.valueOf(missions.get(0).getBudget()) + " €");
        duration = findViewById(R.id.duration_mission_pro);
        duration.setText(String.valueOf(missions.get(0).getDuration()) + " jours");
        date = findViewById(R.id.date_mission_pro);
        date.setText(String.valueOf(missions.get(0).getCreationDate()));
        isAvailable = findViewById(R.id.isAvailable_mission_pro);

        this.missionIsAvailable = missions.get(0).isAvailable();
        if (missionIsAvailable) {
            isAvailable.setBackgroundResource(R.drawable.circle_available);
        } else {
            isAvailable.setBackgroundResource(R.drawable.circle_not_available);
        }

        categories = findViewById(R.id.categories_mission_pro);
        Category[] categoriesList = missions.get(0).getCategories();
        String contentCategories = "";
        int index = 0;
        for (Category category : categoriesList) {
            if (index == categoriesList.length - 1) {
                contentCategories+= category.getLabel();
            } else {
                contentCategories+= category.getLabel() + " - ";
            }
            index++;
        }
        categories.setText(contentCategories);

        btnUsersPostuled = findViewById(R.id.btn_postuled_mission_pro);
        this.listUsersPostuled = missions.get(0).getUserPostuled();
        String content = "";
        if (listUsersPostuled.length > 1) {
            content = "Voir les " + listUsersPostuled.length + " candidatures en cours";
        } else if (listUsersPostuled.length == 1) {
            content = "Voir la candidature en cours";
        } else {
            content = "Aucune candidature en cours";
        }
        btnUsersPostuled.setText(content);

        Button btnProfilAcceptedFreelance = findViewById(R.id.btn_profil_accepted);

        if (!missionIsAvailable) {
            btnProfilAcceptedFreelance.setText("Voir le profil de " + missions.get(0).getUserAccepted());
            btnUsersPostuled.setVisibility(View.INVISIBLE);
            btnUsersPostuled.setHeight(0);
            RelativeLayout map = findViewById(R.id.map);
            map.setVisibility(View.INVISIBLE);
            map.getLayoutParams().height = 0;
        }

        if (missionIsAvailable) {
            btnProfilAcceptedFreelance.setVisibility(View.INVISIBLE);
            btnProfilAcceptedFreelance.setHeight(0);
        }
    }

    private void onClickDeleteMission(int idMission) {
        System.out.println("DELETE-------------------------------------------------------------");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
        Call<Void> call = api.deleteMission(idMission);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
        Toast.makeText(getApplicationContext(), "Mission supprimée", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ListMissionPro.class);
        startActivity(intent);
    }

    private void onClickRedirectToListUsersHavePostuled() {
        if (listUsersPostuled.length > 0) {
            Intent intent = new Intent(this, ListUsersHavePostuled.class);
            String data = "";
            for (int i = 0; i< listUsersPostuled.length; i++) {
                data += listUsersPostuled[i].getIdFreelance() + "-" + listUsersPostuled[i].getIdMission() + "-" + listUsersPostuled[i].getName() + "-" + listUsersPostuled[i].getProposalPrice() + "-" + listUsersPostuled[i].getProposalDate() + "_";
            }
            intent.putExtra("POSTULED", data);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Veuillez patienter des candidatures vont arriver.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_mission_pro);
        Intent i = this.getIntent();
        final int currentIdMission = Integer.parseInt(i.getExtras().getString("idMission"));
        getMissionToDisplay(currentIdMission);

        // DELETE MISSION
        Button btnDelete = findViewById(R.id.btn_delete_mission);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickDeleteMission(currentIdMission);
            }
        });

        // REDIRECT TO DISPLAY ACTIVITY OF USERS HAVE POSTULED
        btnUsersPostuled = findViewById(R.id.btn_postuled_mission_pro);
        btnUsersPostuled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRedirectToListUsersHavePostuled();
            };
        });
    }
}


