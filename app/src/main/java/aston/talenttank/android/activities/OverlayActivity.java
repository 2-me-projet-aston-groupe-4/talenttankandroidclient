package aston.talenttank.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import aston.talenttank.android.R;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.components.ui.login.LoginActivity;
import aston.talenttank.android.components.data.model.LoggedInUser;
import aston.talenttank.android.utils.Constant;


public class OverlayActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    static LoginRepository loginRepository = LoginRepository.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_page);
        drawerLayout = findViewById(R.id.drawer_layout);
    }
    public void ClickMenu (View view){
        openDrawer(drawerLayout);
    }

    public static void openDrawer(DrawerLayout drawerLayout) {
        System.out.println("Open Drawer Role : " + loginRepository.getUser().getRole());
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void ClickLogo(){
        closeDrawer(drawerLayout);
    }

    public static void closeDrawer(DrawerLayout drawerLayout) {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            //close drawer when its open
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickHome (View view) {
        recreate();
    }



    public void ClickMissions (View view) {
        //redirectActivity(this, ListMission.class);
    }



    public void ClickProfiles (View view) {

        redirectActivity(this, ListProfile.class);
    }

    public void ClickAboutMe (View view) {
        //redirectActivity(this,);
        System.out.println("WIP");
    }

    public void ClickLogout (View view) {
        OverlayActivity.closeDrawer(drawerLayout);
        LoginRepository loginRepository = LoginRepository.getInstance();
        loginRepository.logout();
        OverlayActivity.redirectActivity(this, LoginActivity.class);
    }

    private void logout(OverlayActivity overlayActivity) {
        System.out.println("WIP - LOGOUT METHOD");
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent i = new Intent(activity, aClass);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(i);
    }

    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }
}
