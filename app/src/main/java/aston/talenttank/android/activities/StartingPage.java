package aston.talenttank.android.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import aston.talenttank.android.R;
import aston.talenttank.android.components.data.LoginRepository;
import aston.talenttank.android.components.ui.login.LoginActivity;
import android.widget.TextView;

import aston.talenttank.android.R;
import aston.talenttank.android.components.data.LoginRepository;

public class StartingPage extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private LoginRepository loginRepository = LoginRepository.getInstance();
    private String userName = loginRepository.getUser().getDisplayName() + " " + loginRepository.getUser().getLastName();
    private String userRole = loginRepository.getUser().getRole();

    private void onClickOpenActivityAddMission() {
        Intent intent = new Intent(this, AddMission.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_page);
        drawerLayout = findViewById(R.id.drawer_layout);

        TextView t1 = findViewById(R.id.name_pro);
        TextView t2 = findViewById(R.id.role_pro);
        t1.setText(userName);
        t2.setText(userRole);

        Button btnAddMission = findViewById(R.id.btn_add_mission);
        if (!LoginRepository.getInstance().getUser().getRole().equals("employer")) {
            btnAddMission.setVisibility(View.INVISIBLE);
            btnAddMission.setHeight(0);
        } else {
            btnAddMission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickOpenActivityAddMission();
                };
            });
        }
    }

    public void ClickMenu (View view){
        OverlayActivity.openDrawer(drawerLayout);
    }

    public void ClickLogout (View view) {
        OverlayActivity.closeDrawer(drawerLayout);
        LoginRepository loginRepository = LoginRepository.getInstance();
        loginRepository.logout();
        OverlayActivity.redirectActivity(this, LoginActivity.class);
    }

    public void ClickHome(View v){

        recreate();
    }

    public void ClickMissions(View v){
        LoginRepository loginRepository = LoginRepository.getInstance();
        String role = loginRepository.getUser().getRole();
        if (role.equals("employer")) {
            OverlayActivity.redirectActivity(this, ListMissionPro.class);
        } else {
            OverlayActivity.redirectActivity(this, ListMission.class);
        }
    }

    public void ClickAboutMe (View v){
        // WIP still redirecting to Starting page
        OverlayActivity.redirectActivity(this, MyAccount.class);
    }

    public void ClickProfiles (View v){
        OverlayActivity.redirectActivity(this, ListProfile.class);
    }
}