package aston.talenttank.android.activities.overlay;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import aston.talenttank.android.R;
import aston.talenttank.android.components.data.LoginRepository;

public class MainOverlayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_overlay);
    }
}